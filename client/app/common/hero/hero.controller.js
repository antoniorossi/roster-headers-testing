var jsSHA = require("jssha");
var moment = require('moment');

class HeroController {
  constructor($http) {
    "ngInject";

    this.name = 'hero';

    this.matches = [];

    this.http = $http;
  }

  standardQuery() {
    var start = moment();
    start.minute(0);
    start.second(0);
    start.millisecond(0);
    var firstParam = start.toISOString();
    var secondParam = start.add(1, 'd').toISOString();

    var query = '?startDate=' + firstParam + '&endDate=' + secondParam;
    return query;
  };

  xdate() {
    var dateUtc = Date.now();
    var shaObj = new jsSHA("SHA-1", "TEXT");
    shaObj.update(dateUtc);
    return shaObj.getHash("HEX");
  };

  getNextMatches() {
    console.log('get next matches');
    console.log(this.xdate());

    var baseUrl = 'http://52.212.170.118';
    var nextMatchesUrl = baseUrl + '/matches' + this.standardQuery();

    var req = {
      method: 'GET',
      url: nextMatchesUrl,
      headers: {
        'X-Date': this.xdate
      }
    };

    this.http(req)
      .then(res => {
        console.log(res);
        this.matches = res.data;
      }, err => {
        console.log(err);
      })
  }

}

export default HeroController;
